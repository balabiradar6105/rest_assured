package restAssured_project;

import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
public class Soap_NumberToDollars {

	public static void main(String[] args) {
		
		//step 1 declare the base URL
		RestAssured.baseURI="https://www.dataaccess.com";
		
		//step 2 declare the request body
		String requestbody="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "   <soapenv:Header/>\r\n"
				+ "   <soapenv:Body>\r\n"
				+ "      <web:NumberToDollars>\r\n"
				+ "         <web:dNum>50</web:dNum>\r\n"
				+ "      </web:NumberToDollars>\r\n"
				+ "   </soapenv:Body>\r\n"
				+ "</soapenv:Envelope>";
		//step 3 trigger the api and fetch responsebody
		
		String responsebody=given().header("Content-Type", "text/xml; charset=utf-8").body(requestbody).
				when().post("/webservicesserver/NumberConversion.wso").then().extract().response().getBody().asString();
		//step 4 print the responsebody
        System.out.println(responsebody);
		//step 5 extract responsebody parameters
        XmlPath xml_res=new XmlPath(responsebody);
        String res_tag=xml_res.getString("NumberToDollarsResult");
        System.out.println("the result is :" +res_tag);
        
        // step 6 validate response body
      Assert.assertEquals(res_tag, "fifty dollars");
        
		

	}

}
