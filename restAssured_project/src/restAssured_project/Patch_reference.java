package restAssured_project;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Patch_reference {

	public static void main(String[] args) {

		// step 1 declare base URL
		RestAssured.baseURI = "https://reqres.in/";

		// step 2 configure request parameter and trigger the api
		String requestbody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		String responsebody = given().header("Content-Type", "application/json").body(requestbody).when()
				.patch("api/users/2").then().log().all().extract().response().asString();
		// System.out.println("responsebody is : " +responsebody );

		// step 3 create the object of json path and parse the requestbody and then responsebody
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		System.out.println("name is :" + res_name);
		String res_job = jsp_res.getString("job");
		System.out.println("job is :" + res_job);
		String res_updatedAt = jsp_res.getString("updatedAt");
		System.out.println("updatedAt is :" + res_updatedAt);
		res_updatedAt = res_updatedAt.substring(0, 11);

        // step 4 validate reponsebody
		Assert.assertEquals(res_name, "morpheus");
		Assert.assertEquals(res_job, "zion resident");
		Assert.assertEquals(res_updatedAt, expecteddate);

	}

}
