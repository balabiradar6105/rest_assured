package restAssured_project;

import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Soap_reference {

	public static void main(String[] args) {
		// step 1 declare the base url
		RestAssured.baseURI = "https://www.dataaccess.com";

		// step 2 declare the request body
		String requestbody = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
				+ "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n" + "  <soap:Body>\r\n"
				+ "    <NumberToWords xmlns=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "      <ubiNum>500</ubiNum>\r\n" + "    </NumberToWords>\r\n" + "  </soap:Body>\r\n"
				+ "</soap:Envelope>";

		// step 3 Trigger the api and fetch the responsebody

String responsebody = given().header("Content-Type", "text/xml; charset=utf-8").body(requestbody).when()
	  .post("/webservicesserver/NumberConversion.wso").then().extract().response().getBody().asString();

      //step 4 print the responsebody
        System.out.println(responsebody);
        
        //step 5 extract the responsebody parameter
        
        XmlPath xml_res=new XmlPath(responsebody);
        
        String res_tag=xml_res.getString("NumberToWordsResult");
        System.out.println("The result is :" +res_tag);
        
        //validate the responsebody
        Assert.assertEquals(res_tag, "five hundred ");
        
        	}

}
