package Test_Case_Packg;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import API_common_Methods.Common_Method_Handle_API;
import Endpoint.Post_endpoint;
import Request_repository.Post_request_repository;
import io.restassured.path.json.JsonPath;
import utility_common_methods.Handle_API_logs;
import utility_common_methods.Handle_directory;

public class Post_TestCase extends Common_Method_Handle_API {
	@Test
	public static void excutor() throws IOException {	
		File log_dir=Handle_directory.Create_log_directory("Post_TestCase.logs");
		String requestbody = Post_request_repository.Post_request_TC1();
		String endpoint = Post_endpoint.Post_endpoint_TC1();
		for (int i = 0; i < 5; i++) {
			int statusCode = Post_statusCode(requestbody, endpoint);
			System.out.println(statusCode);
			if (statusCode == 201) {
				String responsebody = Post_responsebody(requestbody, endpoint);
				System.out.println(responsebody);
				Handle_API_logs.evidence_creator(log_dir,"Post_TestCase" ,  endpoint, requestbody, responsebody);
				Post_TestCase.validator(requestbody, responsebody);
				break;
			} else {
				System.out.println("expected status code not found hence retrying");
			}
		}
	}

	public static void validator(String requsetbody, String responsebody) {
		JsonPath jsp_req = new JsonPath(requsetbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_id = jsp_res.getString("id");
		System.out.println("id is :" + res_id);
		String res_name = jsp_res.getString("name");
		System.out.println("name is :" + res_name);
		String res_job = jsp_res.getString("job");
		System.out.println("job is :" + res_job);
		String res_createdAt = jsp_res.getString("createdAt");
		System.out.println("createdAt is :" + res_createdAt);
		res_createdAt = res_createdAt.substring(0, 11);

		AssertJUnit.assertEquals(res_name, "morpheus");
		AssertJUnit.assertEquals(res_job, "leader");
		Assert.assertNotNull(res_id);
		AssertJUnit.assertEquals(res_createdAt, expecteddate);

	}

}
